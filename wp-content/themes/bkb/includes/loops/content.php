<?php
/*
The Default Loop (used by index.php and category.php)
=====================================================

If you require only post excerpts to be shown in index and category pages, then use the [---more---] line within blog posts.

If you require different templates for different post types, then simply duplicate this template, save the copy as, e.g. "content-aside.php", and modify it the way you like it. (The function-call "get_post_format()" within index.php, category.php and single.php will redirect WordPress to use your custom content template.)

Alternatively, notice that index.php, category.php and single.php have a post_class() function-call that inserts different classes for different post types into the section tag (e.g. <section id="" class="format-aside">). Therefore you can simply use e.g. .format-aside {your styles} in css/bst.css style the different formats in different ways.
*/
?>
<script type="text/javascript">
var arrayP = []
</script>
    <?php 
  $linkf=1;
  $fp=0;
    if(have_posts()): while(have_posts()): the_post(); 
    $fp++;

    if($fp==1){
      $primer=$post->ID;
    }

    $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
    if($url!=""){
        $st='style="background-image: url('.$url.')"';
    }
    $c1=get_field('color1');
    $c2=get_field('color2');
    if($c1==""){
      $c1="rgba(33, 179, 228, .6)";
    }
    if($c2==""){
      $c2="rgba(35, 166, 95, .6)";
    }
    $ss='style=\'background: '.$c1.';
background: -moz-linear-gradient(left, '.$c1.' 0%, '.$c2.' 100%);
background: -webkit-linear-gradient(left, '.$c1.' 0%,'.$c2.' 100%);
background: linear-gradient(to right, '.$c1.' 0%,'.$c2.' 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="'.$c1.'", endColorstr="'.$c2.'",GradientType=1 ); \'';
    ?>
    <div class="swiper-slide" >

            <div class="head"><span><?php the_title();?></span> <img src="<?php bloginfo('template_directory')?>/images/logo1x.png" srcset="<?php bloginfo('template_directory')?>/images/logo1x.png 1x, <?php bloginfo('template_directory')?>/images/logo2x.png 2x" alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' class="logo-head"></div>

      <div class="swiper-container nido swiper-container-<?php echo $post->ID?>" data-swip="swiperV<?php echo $post->ID?>">
      <!-- <div class="head"><?php the_title(); echo get_menu_swipe();?></div> -->
          <div class="swiper-wrapper">


            <div class="swiper-slide fslider" <?php echo $st?>>
              <div class="flecha"></div>
            <div class="box-center" >  

             <div class="box-grad" <?php echo $ss?>> 
             <div class="cont"> 
              <article role="article" id="post_<?php the_ID()?>" <?php post_class()?>>
                  <h2 class="title">
                      <span class="line"><?php the_title()?></span>
                  </h2>
                  <?php the_content();?>

              </article>
             </div>
             </div> 
            </div> 
            </div> 
            
            <?php 
            
            
            if( have_rows('layer_style') ): 
            while( have_rows('layer_style') ): the_row(); 
            
            switch (get_sub_field('options_l')) {
              case 'box-text':
                  
                    $title = get_sub_field('v1_box1');
                    $content = get_sub_field('v2_box1');
                    $bg = get_sub_field('v3_box1');

                    if($bg=="image"){
                      
                      $tss='style="background-image: url('.get_sub_field('v44_box1', $post->ID).')"';
                      $extra="extra";
                    }elseif ($bg=="gradient") {
                      $tc1=get_sub_field('v5_box1');
                      $tc2=get_sub_field('v6_box1');
                      if($tc1==""){
                        $tc1="rgba(33, 179, 228, .6)";
                      }
                      if($tc2==""){
                        $tc2="rgba(35, 166, 95, .6)";
                      }
                       $extra="";
                      $tss='style=\'background: '.$tc1.';
                  background: -moz-linear-gradient(left, '.$tc1.' 0%, '.$tc2.' 100%);
                  background: -webkit-linear-gradient(left, '.$tc1.' 0%,'.$tc2.' 100%);
                  background: linear-gradient(to right, '.$tc1.' 0%,'.$tc2.' 100%);
                  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="'.$c1.'", endColorstr="'.$c2.'",GradientType=1 ); \'';
                    }
                    ?>
                    <div class="swiper-slide <?php echo get_sub_field('options_l'); echo " ".$extra;?>" <?php echo $tss?>>
                      <div class="cont aleft">
                        <h3><?php echo $title;?></h3>
                        <div class="text"><?php echo $content;?></div>
                      </div>
                    </div>
                    <?php

                break;

            case 'box-qoute':
                  
                    $title2 = get_sub_field('v1_box2');
                    $size = get_sub_field('v6_box2');
                    $bg2 = get_sub_field('v2_box2');
                    $tss2="";
                    if($bg2=="image"){
                      
                      $tss2='style="background-image: url('.get_sub_field('v3_box2', $post->ID).')"';
                      $extra2="extra";
                    }elseif ($bg2=="gradient") {
                      $tc12=get_sub_field('v4_box2');
                      $tc22=get_sub_field('v5_box2');
                      if($tc12==""){
                        $tc12="rgba(33, 179, 228, .6)";
                      }
                      if($tc22==""){
                        $tc22="rgba(35, 166, 95, .6)";
                      }
                       $extra2="";
                      $tss2='style=\'background: '.$tc12.';
                  background: -moz-linear-gradient(left, '.$tc12.' 0%, '.$tc22.' 100%);
                  background: -webkit-linear-gradient(left, '.$tc12.' 0%,'.$tc22.' 100%);
                  background: linear-gradient(to right, '.$tc12.' 0%,'.$tc22.' 100%);
                  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="'.$tc12.'", endColorstr="'.$tc22.'",GradientType=1 ); \'';
                    }
                    ?>
                    <div class="swiper-slide <?php echo get_sub_field('options_l'); echo " ".$extra2;?>" <?php echo $tss2?>>
                      <div class="cont aleft">
                        <h3 class="<?php echo $size;?>"><?php echo $title2;?></h3>
                      </div>
                    </div>
                    <?php

                break;   

                case 'box-fin':
                    $linkf++; 
                    $adjacent_post = get_adjacent_post(true,'',true) ;
                      
                    if($adjacent_post){
                    $my_id = $adjacent_post->ID;
                    }
                    else{
                      $my_id=$primer;
                      $linkf=1;
                    }
                    $post_id = get_post($my_id);
                    $content = $post_id->post_content;
                    $content = apply_filters('the_content', $content);
                    $content = str_replace(']]>', ']]>', $content);

                    $title3 = get_sub_field('v1_box3');
                    $content3 = get_sub_field('v2_box3');                                
                    $tss3='style="background-image: url('.get_field('final-image', $post_id).')"';
                    
                    $tc13=get_field('color1', $post_id);
                    $tc23=get_field('color2', $post_id);
                      if($tc13==""){
                        $tc13="rgba(33, 179, 228, .6)";
                      }
                      if($tc23==""){
                        $tc23="rgba(35, 166, 95, .6)";
                      }
                  $tss4='style=\'background: '.$tc13.';
                  background: -moz-linear-gradient(left, '.$tc13.' 0%, '.$tc23.' 100%);
                  background: -webkit-linear-gradient(left, '.$tc13.' 0%,'.$tc23.' 100%);
                  background: linear-gradient(to right, '.$tc13.' 0%,'.$tc23.' 100%);
                  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="'.$tc13.'", endColorstr="'.$tc23.'",GradientType=1 ); \'';  

                    ?>
                    <div class="swiper-slide lslider <?php echo get_sub_field('options_l');?>" <?php echo $tss3?>>
                     <div class="final-fin">
                     <div class="box-grad lastgrad" <?php echo $tss4?> data-slide=<?php echo $linkf; ?> >
                      <div class="cont">
                        <h2 class="title">
                          <span class="line"><?php echo $post_id->post_title; ?></span>
                        </h2>
                        <?php  echo $content; ?>
                      </div>
                      </div>
                      <div class="foot">
                        <?php echo get_sub_field('v4_box3'); ?>
                      </div>
                    </div>
                    </div>
                    <?php
                  
                break; 
              
              default:
                # code...
                break;
            }

              

              ?>

              

            <?php endwhile; ?>

           

          <?php endif; ?>
          </div>
          <div class="pagination swiper-pagination swiper-pagination-<?php echo $post->ID?>"></div>
          <div class="sn swiper-button-next-<?php echo $post->ID?>">></div>
          <div class="sp swiper-button-prev-<?php echo $post->ID?>"><</div>
      
    <script type="text/javascript">

    var swiperV<?php echo $post->ID?> = new Swiper('.swiper-container-<?php echo $post->ID?>', {
        pagination: '.pagination.swiper-pagination-<?php echo $post->ID?>',
        paginationClickable: true,
        keyboardControl:true,
        speed: 700,
        centeredSlides: true,
        slidesPerView: 'auto',
        observer:true,
        onProgress: function(e){
       if(e.isEnd){
             jQuery('.head span, .pagination').css('opacity','0')
        } else {
             jQuery('.head span, .pagination').css('opacity','1')
        }
    }
        
     
    });
    arrayP.push(swiperV<?php echo $post->ID?>)

    jQuery('.swiper-button-prev-<?php echo $post->ID?>').on('click', function(e){
      console.log('prev<?php echo $post->ID?>')
      //e.preventDefault()
      swiperV<?php echo $post->ID?>.slidePrev()
    })
    jQuery('.swiper-button-next-<?php echo $post->ID?>, .fslider').on('click', function(e){
            console.log('next<?php echo $post->ID?>')

      //e.preventDefault()
      swiperV<?php echo $post->ID?>.slideNext()
    })
     
    </script>
    </div>
    </div> 
    <?php comments_template('/includes/loops/comments.php'); ?>
    <?php endwhile; ?>
    <?php else: get_template_part('includes/loops/content', 'none'); ?>
    <?php endif; ?>
   
