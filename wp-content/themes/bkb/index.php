<?php get_template_part('includes/header'); ?>

<div class="container-fluid" id="cont-master">
      <div id="content" role="main">		
       <div class="swiper-container swiper-cont">
        <?php
          $my_id = 2;
          $post_id = get_post($my_id);
          $content = $post_id->post_content;
          $content = apply_filters('the_content', $content);
          $content = str_replace(']]>', ']]>', $content);
          $url = wp_get_attachment_url( get_post_thumbnail_id($my_id) );
          if($url!=""){
              $st='style="background-image: url('.$url.')"';
          }
        ?>
        <div class="swiper-wrapper">
          <div class="swiper-slide" <?php echo $st?>>
          <div id="landing" class="box-landing">
            <div class="box-center landing">
              <div class="cont">
                <img src="<?php bloginfo('template_directory')?>/images/logo.png" class="logo">
                <?php echo $content; ?>
                <img src="/wp-content/themes/bkb/images/arrows.png" class="more lift">
              </div>
            </div>
          </div>
          </div>
          

          <?php get_template_part('includes/loops/content', get_post_format()); ?>
        </div>
        </div>
      </div><!-- /#content -->
</div><!-- /.container -->

<?php get_template_part('includes/footer'); ?>
