<?php
/*
All the functions are in the PHP pages in the functions/ folder.
*/

require_once locate_template('/functions/cleanup.php');
require_once locate_template('/functions/setup.php');
require_once locate_template('/functions/enqueues.php');
require_once locate_template('/functions/navbar.php');
require_once locate_template('/functions/widgets.php');
require_once locate_template('/functions/search.php');
require_once locate_template('/functions/feedback.php');

add_action('after_setup_theme', 'true_load_theme_textdomain');

function true_load_theme_textdomain(){
    load_theme_textdomain( 'bst', get_template_directory() . '/languages' );
}

function get_menu_swipe(){
			$args= array(
					'posts_per_page' => 10000,
					'order'	=>'DESC'
				);
			$the_query = new WP_Query( $args );

			// The Loop
			if ( $the_query->have_posts() ) {
				?>
				<div class="full-menu">
					<span class="open-m">Menu</span>
					<ul class="menu-u">
					
				<?php
				$i=1;
				while ( $the_query->have_posts() ) {
					$the_query->the_post();
					
					?>
						<li data-slide="<?php echo $i;?>"><?php the_title();?></li>
					<?php
					$i++;
					}
					?>
					</ul>
				</div>	
				
			<?php }
			wp_reset_query();
}

add_filter('show_admin_bar', '__return_false');